import axios from 'axios';
const baseUrl = 'https://todo-ssu-api.herokuapp.com';

const convertDbToUi = (dbTodo) => ({
    name: dbTodo.name,
    description: dbTodo.description,
    isDone: dbTodo.is_done,
    id: dbTodo._id
});

const convertUiToDb = (todo) => ({
    name: todo.name,
    description: todo.description,
    is_done: todo.isDone,
    _id: todo.id
})

export async function getTodos(filter) {
    const res = await axios.get(`${baseUrl}/todos`);
    const todos = res.data.map(todo => convertDbToUi(todo));
    return todos;
}

export async function createTodo(todo) {
    const res = await axios.post(`${baseUrl}/todos`, convertUiToDb(todo));
    return convertDbToUi(res.data);
}

export async function updateTodo(todo) {
    const res = await axios.put(`${baseUrl}/todos/${todo.id}`, convertUiToDb(todo));
    return convertDbToUi(res.data);
}

