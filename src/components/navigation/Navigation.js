import React from 'react';
import { Link } from 'react-router-dom';
import { Nav, NavItem } from 'reactstrap';
import './Navigation.css';

export const Navigation = () => (
  <Nav vertical pills>
    <NavItem>
      <Link to="/" className="nav-link">All</Link>
    </NavItem>
    <NavItem>
      <Link to="/todo" className="nav-link">To Do</Link>
    </NavItem>
    <NavItem>
      <Link to="/done" className="nav-link">Done</Link>
    </NavItem>
  </Nav>
);
