import React from 'react';
import { Navbar, NavbarBrand } from 'reactstrap';

export const Header = () => (
    <Navbar color="light" light expand="md">
        <NavbarBrand href="/">ToDo App</NavbarBrand>
    </Navbar>
);
