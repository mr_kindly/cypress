import React, { Component } from 'react';
import { Modal, ModalBody, ModalHeader, ModalFooter, Button, Form, FormGroup, Label, Input } from 'reactstrap';
import { connect } from 'react-redux';
import { addTodo } from '../../store/actions';


class CreateModal extends Component {

    constructor(props) {
        super(props);
        this.state = {
            name: '',
            description: ''
        };
    }

    add = () => {
        const { name, description } = this.state;
        const { dispatch, toggle } = this.props;
        dispatch(addTodo({ name, description })).then(() => {
            toggle();
            this.clear();
        });
    };

    cancel = () => {
        const { toggle } = this.props;
        toggle();
        this.clear();
    }

    handleChange = (event, prop) => {
        this.setState({ [prop]: event.target.value });
    };

    clear = () => this.setState({name: '', description: ''});

    render() {
        const { isOpen } = this.props;
        const { name, description } = this.state;
        return (<Modal isOpen={isOpen} toggle={this.cancel}>
            <ModalHeader toggle={this.cancel}>Add new todo</ModalHeader>
            <ModalBody>
                <Form>
                    <FormGroup>
                        <Label for="name">Name</Label>
                        <Input type="text" name="name" id="name" value={name} onChange={(event) => this.handleChange(event, 'name')} placeholder="Please type name" />
                    </FormGroup>
                    <FormGroup>
                        <Label for="description">Description</Label>
                        <Input type="description" name="description" value={description} onChange={(event) => this.handleChange(event, 'description')} id="description" placeholder="Please type description" />
                    </FormGroup>
                </Form>
            </ModalBody>
            <ModalFooter>
                <Button id="add" color="success" onClick={this.add}>Add</Button>{' '}
                <Button color="secondary" onClick={this.cancel}>Cancel</Button>
            </ModalFooter>
        </Modal>);
    }
}

export default connect(null, null)(CreateModal);
