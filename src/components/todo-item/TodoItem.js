import React, { Component } from 'react';
import { Input } from 'reactstrap';
import { connect } from 'react-redux';
import { updateTodo } from '../../store/actions';

class TodoItem extends Component {
    constructor(props) {
        super(props);
    }

    handleChange = (event) => {
        const { dispatch, name, description, id } = this.props;
        const todo = {
            id, name, description, isDone: event.target.checked
        }
        dispatch(updateTodo(todo));
    };

    render() {
        const { number, name, description, isDone, key } = this.props;
        return (<tr key={key}>
            <th scope="row">{number}</th>
            <td>{name}</td>
            <td>{description}</td>
            <td>
                <Input type="checkbox" checked={isDone} onChange={this.handleChange}/>
            </td>
        </tr>);
    }
}

export default connect(null, null)(TodoItem);

// export default () => (<h2>Test</h2>);
