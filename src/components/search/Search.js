import React from 'react';
import { Form, FormGroup, InputGroup, InputGroupAddon, Button, Input} from 'reactstrap';

export const Search = () => (
    <Form>
        <FormGroup>
            <InputGroup>
                <Input id="search-input"/>
                <InputGroupAddon addonType="append">
                    <Button id="search-btn" color="success">Search</Button>
                </InputGroupAddon>
            </InputGroup>
        </FormGroup>
    </Form>
);
