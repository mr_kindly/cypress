import React from 'react';
import { Table } from 'reactstrap';
import { default as TodoItem } from './../todo-item/TodoItem';

export const TodoList = ({todos = []}) => {
    return ( todos.length === 0 ? <h3>Todo list is empty! </h3> :
        <React.Fragment>
            <p><strong>Total items: </strong> <span id="total-items">{todos.length}</span></p>
            <Table hover>
                <thead>
                    <tr>
                        <th>#</th>
                        <th>Name</th>
                        <th>Description</th>
                        <th></th>
                    </tr>
                </thead>
                <tbody>
                    {
                        todos.map((todo, index) => (
                            <TodoItem key={index} name={todo.name} description={todo.description} id={todo.id} number={index + 1} isDone={todo.isDone}/>
                        ))
                    }
                </tbody>
            </Table>
        </React.Fragment>
    )
};
