
import * as types from './action-types';
import { getTodos, createTodo, updateTodo as update } from './../services/api';

export function fetchTodos() {
    return async (dispatch) => {
        try {
            const todos = await getTodos();
            dispatch({ type: types.TODOS_FETCHED, todos });
        } catch (error) {
            console.error(error);
        }
    };
};

export const addTodo = (todo) => {
    return async (dispatch, state) => {
        try {
            const newTodo = await createTodo(todo);
            return dispatch(fetchTodos());
        } catch (error) {
            console.error(error);
        }
    }
};

export const updateTodo = (todo) => {
    return async (dispatch, state) => {
        try {
            const updatedTodo = await update(todo);
            return dispatch(fetchTodos());
        } catch (error) {
            console.error(error);
        }
    }
}
