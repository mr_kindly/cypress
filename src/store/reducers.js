import _ from 'lodash';
import * as types from './action-types';

const initialState = {
  todos: []
};

export default function reduce(state = initialState, action = {}) {
  switch (action.type) {
    case types.TODOS_FETCHED:
      return {
        todos: action.todos
      };
    default:
      return state;
  }
};