export const FILTER_CHANGED = 'FILTER_CHANGED';
export const ADD_TODO = 'ADD_TODO';
export const GET_TODOS = 'GET_TODOS';
export const TODOS_FETCHED = 'TODOS_FETCHED';
