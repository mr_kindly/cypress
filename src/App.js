import React from 'react';
import { Container, Row, Col, Button } from 'reactstrap';
import { Navigation, Header, Search, CreateModal, Filter } from './components';


class App extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      isOpenCreateModal: false,
      isOpenFilter: false
    }
  }

  toggleCreateTodo = () => {
    this.setState({
      isOpenCreateModal: !this.state.isOpenCreateModal
    });
  }

  toggleFilter = () => {
    this.setState({
      isOpenFilter: !this.state.isOpenFilter
    });
  }

  render() {
    return (
      <React.Fragment>
        <Container>
          <Row>
            <Col xs="12">
              <Header />
            </Col>
          </Row>
          <Row>
            <Col xs="3">
              <Navigation />
            </Col>
            <Col xs="9">
              <Row>
                <Col xs="9">
                  <Search />
                </Col>
                <Col xs="3">
                  <Button color="primary" onClick={this.toggleFilter} id="filter-btn">Filter</Button>{"  "}
                  <Button color="success" onClick={this.toggleCreateTodo} id="add-btn">Add Todo</Button>
                </Col>
              </Row>
              <Row>
                {this.props.children}
              </Row>
            </Col>
          </Row>
        </Container>
        <CreateModal isOpen={this.state.isOpenCreateModal} toggle={this.toggleCreateTodo} />
        <Filter isOpen={this.state.isOpenFilter} toggle={this.toggleFilter} />
      </React.Fragment>
    );
  }
}

export default App;
