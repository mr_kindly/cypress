import React, {Component} from 'react';
import { TodoList } from './../../components';
import { connect } from 'react-redux';
import { fetchTodos } from '../../store/actions';

class Todo extends Component {

    constructor(props) {
        super(props);
    }

    componentDidMount() {
        this.props.dispatch(fetchTodos());
    }

    render() {
        return (<TodoList todos={this.props.todos}/>);
    }

}

// which props do we want to inject, given the global store state?
// always use selectors here and avoid accessing the state directly
function mapStateToProps(state) {
    const todos = state.default.todos;
    
    return {
        todos: todos.filter(todo => !todo.isDone)
    };
}

export default connect(mapStateToProps)(Todo);