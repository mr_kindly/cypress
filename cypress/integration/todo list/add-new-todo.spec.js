/// <reference types="Cypress" />

context('First test', () => {
  beforeEach(() => {
    cy.visit('http://localhost:3000');
    
  });

  xit('Add new todo with server request', () => {
    cy.server()
    cy.route({
      method: 'POST',
      url: '/todos',
    }).as('addTodos');

    cy.route({
      method: 'GET',
      url: '/todos',
    }).as('getTodos');
    let count;
    cy.get('#add-btn').click();
    cy.get('.modal-dialog');
    cy.get('#name').type('Use cypress in your project');
    cy.get('#description').type('Install, run, write first test, fun!');
    cy.get('#add').click();
    
    cy.wait('@addTodos').then((xhr) => {
      assert.isNotNull(xhr.response.body);
    });
    cy.wait('@getTodos').then((xhr) => {
      assert.isNotNull(xhr.response.body);
      count = xhr.response.body.length;
      cy.get('#total-items').should('have.text', `${count}`);
    });
  });

  it('Add new todo with server request', () => {
    cy.server()
    let count;
    cy.route({
      method: 'GET',      // Route all GET requests
      url: '/todos',    // that have a URL that matches '/users/*'
      response: {body: []}       // and force the response to be: []
    }).as('getTodosStub');
    cy.route({
      method: 'POST',      // Route all GET requests
      url: '/todos',    // that have a URL that matches '/users/*'
      response: {}        // and force the response to be: []
    }).as('addTodosStub')
    
    cy.get('#add-btn').click();
    cy.get('.modal-dialog');
    cy.get('#name').type('Use cypress in your project');
    cy.get('#description').type('Install, run, write first test, fun!');
    cy.get('#add').click();
    
    cy.wait('@addTodosStub').then((xhr) => {
      assert.isNotNull(xhr.response.body);
    });
    cy.wait('@getTodosStub').then((xhr) => {
      assert.isNotNull(xhr.response.body);
      count = xhr.response.body.length;
      cy.get('#total-items').should('have.text', `${count}`);
    });
  });
})
